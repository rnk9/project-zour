class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :timeoutable, :lockable
         
  validates :username, presence: true,
                       length: { in: 4..25 }#,
                       #uniqueness: { case_sensitive: true }
                       
  def admin?
    self.admin
  end
end
